module org.refcodes.security {
	requires org.refcodes.data;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.mixin;
	requires java.logging;
	requires org.refcodes.runtime;

	exports org.refcodes.security;
}
