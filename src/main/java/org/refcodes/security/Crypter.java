// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

/**
 * The {@link Crypter} combines {@link Encrypter} and {@link Decrypter}
 * functionality.
 * 
 * @param <ENC> The type of the encrypted data.
 * @param <DEC> The type of the decrypted data.
 * @param <ENCEXC> The specific type of the {@link EncryptionException} being
 *        thrown.
 * @param <DECEXC> The specific type of the {@link DecryptionException} being
 *        thrown.
 */
public interface Crypter<ENC, DEC, ENCEXC extends EncryptionException, DECEXC extends DecryptionException> extends Encrypter<ENC, DEC, ENCEXC>, Decrypter<ENC, DEC, DECEXC> {

}
