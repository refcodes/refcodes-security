// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

import java.io.IOException;
import java.io.InputStream;

import org.refcodes.mixin.Disposable;

/**
 * A {@link DecryptionInputStream} wraps an {@link InputStream} and consumes
 * input bytes by applying a {@link Decrypter} on each byte read from the
 * provided {@link InputStream} before passing back the processed byte to the
 * caller. The input of the {@link DecryptionInputStream} can be converted back
 * by the according {@link EncryptionOutputStream}.
 */
public class DecryptionInputStream extends InputStream implements Disposable {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private InputStream _inputStream;
	private Decrypter<byte[], byte[], DecryptionException> _decrypter;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link DecryptionInputStream} by wrapping the given
	 * {@link InputStream} for the provided {@link Decrypter} to be applied on
	 * the bytes to be read.
	 * 
	 * @param aInputStream The {@link InputStream} to be wrapped.
	 * @param aDecrypter The {@link Decrypter} to be applied to the bytes to be
	 *        read.
	 */
	public DecryptionInputStream( InputStream aInputStream, Decrypter<byte[], byte[], DecryptionException> aDecrypter ) {
		_inputStream = aInputStream;
		_decrypter = aDecrypter;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _inputStream.available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object aObj ) {
		return _inputStream.equals( aObj );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return _inputStream.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mark( int aReadlimit ) {
		_inputStream.mark( aReadlimit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean markSupported() {
		return _inputStream.markSupported();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException {
		try {
			final int theRead = _inputStream.read();
			if ( theRead == -1 ) {
				return -1;
			}
			return Byte.toUnsignedInt( _decrypter.toDecrypted( new byte[] { (byte) theRead } )[0] );
		}
		catch ( DecryptionException e ) {
			throw new IllegalStateException( "Encountered an illegal sate while decrypting!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read( byte[] b ) throws IOException {
		try {
			final int theRead = _inputStream.read( b );
			if ( theRead == -1 ) {
				return -1;
			}
			_decrypter.decrypt( b );
			return theRead;
		}
		catch ( DecryptionException e ) {
			throw new IllegalStateException( "Encountered an illegal sate while decrypting!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read( byte[] b, int off, int len ) throws IOException {
		try {
			final int theRead = _inputStream.read( b, off, len );
			_decrypter.decrypt( b, off, len );
			return theRead;
		}
		catch ( DecryptionException e ) {
			throw new IllegalStateException( "Encountered an illegal sate while decrypting!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() throws IOException {
		_inputStream.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long skip( long aArg0 ) throws IOException {
		return _inputStream.skip( aArg0 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		final Disposable theDecrypter = _decrypter;
		if ( theDecrypter != null ) {
			try {
				theDecrypter.dispose();
			}
			catch ( Exception ignore ) {}
			_decrypter = null;
		}
		final InputStream theInputStream = _inputStream;
		if ( theInputStream != null ) {
			theInputStream.close();
			_inputStream = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		final Disposable theDecrypter = _decrypter;
		if ( theDecrypter != null ) {
			try {
				theDecrypter.dispose();
			}
			catch ( Exception ignore ) {}
			_decrypter = null;
		}
		final InputStream theInputStream = _inputStream;
		if ( theInputStream != null ) {
			try {
				theInputStream.close();
			}
			catch ( IOException ignore ) { /* ignore */ }
			_inputStream = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [inputStream=" + _inputStream + ", decrypter=" + _decrypter + "]";
	}
}
