// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

import org.refcodes.mixin.EncodingAccessor;

/**
 * Thrown in case an encryption issue occurred regarding the encoding.
 */
public class EncryptEncodingException extends EncryptionException implements EncodingAccessor<String> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected String _encoding;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEncoding The encoding involved in this exception.
	 */
	public EncryptEncodingException( String aMessage, String aEncoding, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_encoding = aEncoding;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEncoding The encoding involved in this exception.
	 */
	public EncryptEncodingException( String aMessage, String aEncoding, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_encoding = aEncoding;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEncoding The encoding involved in this exception.
	 */
	public EncryptEncodingException( String aMessage, String aEncoding, Throwable aCause ) {
		super( aMessage, aCause );
		_encoding = aEncoding;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEncoding The encoding involved in this exception.
	 */
	public EncryptEncodingException( String aMessage, String aEncoding ) {
		super( aMessage );
		_encoding = aEncoding;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEncoding The encoding involved in this exception.
	 */
	public EncryptEncodingException( String aEncoding, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_encoding = aEncoding;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEncoding The encoding involved in this exception.
	 */
	public EncryptEncodingException( String aEncoding, Throwable aCause ) {
		super( aCause );
		_encoding = aEncoding;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEncoding() {
		return _encoding;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getPatternArguments() {
		return new Object[] { _encoding };
	}
}
