// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

import javax.crypto.CipherSpi;

import org.refcodes.mixin.Disposable;

/**
 * Plain interface for providing straight forward encryption functionality as of
 * {@link #toEncrypted(Object)} and for forcing your plain functionality to
 * provide a bridge to the Java Cryptographic Extension (JCE) framework's
 * {@link CipherSpi} as of {@link #toEncrypted(byte[], int, int, byte[], int)}.
 * This way you can use your algorithms outside the JCE framework. This may be
 * necessary when your Java's security settings prevent running your own JCE
 * extensions from inside an (Oracle-) unsigned JAR.
 *
 * @param <ENC> The type of the encrypted data.
 * @param <DEC> The type of the decrypted data.
 * @param <EXC> The specific type of the {@link EncryptionException} being
 *        thrown.
 */
public interface Encrypter<ENC, DEC, EXC extends EncryptionException> extends Disposable {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * You pass in data of a given type and you get encrypted data of (another)
	 * given type.
	 * 
	 * @param aInput The input data to be encrypted.
	 * 
	 * @return The encrypted output data.
	 * 
	 * @throws EXC Thrown in case something went wrong upon encryption.
	 */
	ENC toEncrypted( DEC aInput ) throws EXC;

	/**
	 * For compatibility with the java.security framework and easily to be
	 * integrated in a sub-class of the {@link CipherSpi}.
	 *
	 * @param aBuffer The input to be encrypted.
	 * @param aOffset The offset to start encryption.
	 * @param aLength The length to be encrypted
	 * @param aOutBuffer The output where to encrypt to.
	 * @param aOutOffset The offset where to start writing the encrypted output.
	 * 
	 * @return The number of bytes encrypted.
	 * 
	 * @throws EncryptionException Thrown in case an encryption issue occurred
	 *         regarding the {@link Encrypter}. Probably the configuration of
	 *         your {@link Encrypter} does not fit the one of the
	 *         {@link Decrypter}.
	 * @throws ArrayIndexOutOfBoundsException In case you provided lengths and
	 *         offsets not fitting with the provided arrays.
	 */
	int toEncrypted( byte[] aBuffer, int aOffset, int aLength, byte[] aOutBuffer, int aOutOffset ) throws EncryptionException;

	/**
	 * This method encrypts the provided buffer beginning at the given offset
	 * and the given number of bytes.
	 *
	 * @param aBuffer The input to be encrypted.
	 * @param aOffset The offset to start encryption.
	 * @param aLength The length to be encrypted
	 * 
	 * @return The number of bytes encrypted.
	 * 
	 * @throws EncryptionException Thrown in case a encryption issue occurred
	 *         regarding the {@link Encrypter}. Probably the configuration of
	 *         your {@link Encrypter} does not fit the one of the
	 *         {@link Encrypter}.
	 * @throws ArrayIndexOutOfBoundsException In case you provided lengths and
	 *         offsets not fitting with the provided arrays.
	 */
	default int encrypt( byte[] aBuffer, int aOffset, int aLength ) throws EncryptionException {
		return toEncrypted( aBuffer, aOffset, aLength, aBuffer, aOffset );
	}

	/**
	 * This method encrypts the provided buffer.
	 *
	 * @param aBuffer The input to be encrypted.
	 * 
	 * @return The number of bytes encrypted.
	 * 
	 * @throws EncryptionException Thrown in case a encryption issue occurred
	 *         regarding the {@link Encrypter}. Probably the configuration of
	 *         your {@link Encrypter} does not fit the one of the
	 *         {@link Encrypter}.
	 * @throws ArrayIndexOutOfBoundsException In case you provided lengths and
	 *         offsets not fitting with the provided arrays.
	 */
	default int encrypt( byte[] aBuffer ) throws EncryptionException {
		return toEncrypted( aBuffer, 0, aBuffer.length, aBuffer, 0 );
	}
}
