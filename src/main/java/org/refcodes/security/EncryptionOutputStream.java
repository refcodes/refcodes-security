// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

import java.io.IOException;
import java.io.OutputStream;

import org.refcodes.mixin.Disposable;

/**
 * A {@link EncryptionOutputStream} wraps an {@link OutputStream} and produces
 * output bytes by applying a {@link Encrypter} on each byte to be written
 * before delegating the processed vale to the given {@link OutputStream}. The
 * output of the {@link EncryptionOutputStream} can be converted back by the
 * according {@link DecryptionInputStream}.
 */
public class EncryptionOutputStream extends OutputStream implements Disposable {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private OutputStream _outputStream;
	private Encrypter<byte[], byte[], EncryptionException> _encrypter;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link EncryptionOutputStream} by wrapping the given
	 * {@link OutputStream} for the provided {@link Encrypter} to be applied on
	 * the bytes to be written.
	 * 
	 * @param aOutputStream The {@link OutputStream} to be wrapped.
	 * @param aEncrypter The {@link Encrypter} to be applied to the bytes to be
	 *        written.
	 */
	public EncryptionOutputStream( OutputStream aOutputStream, Encrypter<byte[], byte[], EncryptionException> aEncrypter ) {
		_outputStream = aOutputStream;
		_encrypter = aEncrypter;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object aObj ) {
		return _outputStream.equals( aObj );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		_outputStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return _outputStream.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write( int value ) throws IOException {
		try {
			_outputStream.write( Byte.toUnsignedInt( _encrypter.toEncrypted( new byte[] { (byte) value } )[0] ) );
		}
		catch ( EncryptionException e ) {
			throw new IllegalStateException( "Encountered a bug while encrypting!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write( byte[] b ) throws IOException {
		try {
			_outputStream.write( _encrypter.toEncrypted( b ) );
		}
		catch ( EncryptionException e ) {
			throw new IllegalStateException( "Encountered an illegal state while encrypting!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write( byte[] b, int off, int len ) throws IOException {
		if ( off == 0 && len == b.length ) {
			write( b );
		}
		else {
			try {
				final byte[] theBuffer = new byte[len];
				_encrypter.toEncrypted( b, off, len, theBuffer, 0 );
				_outputStream.write( theBuffer );
			}
			catch ( EncryptionException e ) {
				throw new IllegalStateException( "Encountered an illegal state while encrypting!", e );
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		final Disposable theEncrypter = _encrypter;
		if ( theEncrypter != null ) {
			try {
				theEncrypter.dispose();
			}
			catch ( Exception ignore ) {}
			_encrypter = null;
		}
		final OutputStream theOutputStream = _outputStream;
		if ( theOutputStream != null ) {
			try {
				theOutputStream.flush();
			}
			catch ( Exception ignore ) {}
			theOutputStream.close();
			_outputStream = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		final Disposable theEncrypter = _encrypter;
		if ( theEncrypter != null ) {
			try {
				theEncrypter.dispose();
			}
			catch ( Exception ignore ) {}
			_encrypter = null;
		}
		final OutputStream theOutputStream = _outputStream;
		if ( theOutputStream != null ) {
			try {
				theOutputStream.close();
			}
			catch ( IOException ignore ) { /* ignore */ }
			_outputStream = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [outputStream=" + _outputStream + ", encrypter=" + _encrypter + "]";
	}
}
