// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

import java.io.File;
import java.security.KeyStore;

/**
 * Describes the data required to access a {@link KeyStore}.The {@link KeyStore}
 * is used to decide which authentication credentials should be sent to the
 * remote host for authentication during SSL handshake. If you are an SSL Server
 * you will use a private key during key exchange algorithm and send
 * certificates corresponding to your public keys to client, this certificate is
 * acquired from {@link KeyStore}.
 */
public interface KeyStoreDescriptor extends TrustStoreDescriptor {

	/**
	 * Retrieves the key's password.
	 * 
	 * @return The key password.
	 */
	String getKeyPassword();

	/**
	 * The mutable {@link KeyStoreDescriptorBuilder} extends the
	 * {@link KeyStoreDescriptor} with manipulator methods.
	 */
	public static interface KeyStoreDescriptorBuilder extends KeyStoreDescriptor, TrustStoreDescriptorBuilder {

		/**
		 * Sets the key's password.
		 * 
		 * @param aKeyPassword The key password.
		 */
		void setKeyPassword( String aKeyPassword );

		/**
		 * Builder for the according setter method.
		 * 
		 * @param aKeyPassword The key password.
		 * 
		 * @return The builder instance as of the builder pattern.
		 */
		default KeyStoreDescriptorBuilder withKeyPassword( String aKeyPassword ) {
			setKeyPassword( aKeyPassword );
			return this;
		}

		/**
		 * Builder for the according setter method.
		 * 
		 * @param aStoreFile The {@link KeyStore} {@link File}.
		 * 
		 * @return The builder instance as of the builder pattern.
		 */
		@Override
		default KeyStoreDescriptorBuilder withStoreFile( File aStoreFile ) {
			setStoreFile( aStoreFile );
			return this;
		}

		/**
		 * Builder for the according setter method.
		 * 
		 * @param aStorePassword The {@link KeyStore} password.
		 * 
		 * @return The builder instance as of the builder pattern.
		 */
		@Override
		default KeyStoreDescriptorBuilder withStorePassword( String aStorePassword ) {
			setStorePassword( aStorePassword );
			return this;
		}

		/**
		 * Builder for the according setter method.
		 * 
		 * @param aStoreType The {@link KeyStore}'s {@link StoreType}.
		 * 
		 * @return The builder instance as of the builder pattern.
		 */
		@Override
		default KeyStoreDescriptorBuilder withStoreType( StoreType aStoreType ) {
			setStoreType( aStoreType );
			return this;
		}
	}
}
