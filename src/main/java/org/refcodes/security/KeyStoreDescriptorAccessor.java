// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

/**
 * Provides an accessor for a {@link KeyStoreDescriptor} property.
 */
public interface KeyStoreDescriptorAccessor {

	/**
	 * Retrieves the {@link KeyStoreDescriptor} from the
	 * {@link KeyStoreDescriptor} property.
	 * 
	 * @return The {@link KeyStoreDescriptor} stored by the
	 *         {@link KeyStoreDescriptor} property.
	 */
	KeyStoreDescriptor getKeyStoreDescriptor();

	/**
	 * Provides a mutator for a {@link KeyStoreDescriptor} property.
	 */
	public interface KeyStoreDescriptorMutator {

		/**
		 * Sets the {@link KeyStoreDescriptor} for the
		 * {@link KeyStoreDescriptor} property.
		 * 
		 * @param aKeyStoreDescriptor The {@link KeyStoreDescriptor} to be
		 *        stored by the {@link KeyStoreDescriptor} property.
		 */
		void setKeyStoreDescriptor( KeyStoreDescriptor aKeyStoreDescriptor );
	}

	/**
	 * Provides a builder method for a {@link KeyStoreDescriptor} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface KeyStoreDescriptorBuilder<B extends KeyStoreDescriptorBuilder<B>> {

		/**
		 * Sets the {@link KeyStoreDescriptor} for the
		 * {@link KeyStoreDescriptor} property.
		 * 
		 * @param aKeyStoreDescriptor The {@link KeyStoreDescriptor} to be
		 *        stored by the {@link KeyStoreDescriptor} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withKeyStoreDescriptor( KeyStoreDescriptor aKeyStoreDescriptor );
	}

	/**
	 * Provides a {@link KeyStoreDescriptor} property.
	 */
	public interface KeyStoreDescriptorProperty extends KeyStoreDescriptorAccessor, KeyStoreDescriptorMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link KeyStoreDescriptor} (setter) as of
		 * {@link #setKeyStoreDescriptor(KeyStoreDescriptor)} and returns the
		 * very same value (getter).
		 * 
		 * @param aKeyStoreDescriptor The {@link KeyStoreDescriptor} to set (via
		 *        {@link #setKeyStoreDescriptor(KeyStoreDescriptor)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default KeyStoreDescriptor letKeyStoreDescriptor( KeyStoreDescriptor aKeyStoreDescriptor ) {
			setKeyStoreDescriptor( aKeyStoreDescriptor );
			return aKeyStoreDescriptor;
		}
	}
}
