// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

import java.io.File;
import java.security.KeyStore;

/**
 * The immutable {@link KeyStoreDescriptorImpl} implements the
 * {@link TrustStoreDescriptor} interface.
 */
public class KeyStoreDescriptorImpl implements KeyStoreDescriptor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private KeyStoreDescriptorBuilder _builder;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link KeyStoreDescriptor} with the data required to access
	 * a {@link KeyStore}. The {@link StoreType} defaults to
	 * {@link StoreType#JKS}.
	 * 
	 * @param aStoreFile The {@link KeyStore} {@link File}.
	 */
	public KeyStoreDescriptorImpl( File aStoreFile ) {
		this( aStoreFile, StoreType.JKS, null, null );
	}

	/**
	 * Constructs a {@link KeyStoreDescriptor} with the data required to access
	 * a {@link KeyStore}.
	 * 
	 * @param aStoreFile The {@link KeyStore} {@link File}.
	 * @param aStoreType The {@link KeyStore}'s {@link StoreType}.
	 */
	public KeyStoreDescriptorImpl( File aStoreFile, StoreType aStoreType ) {
		this( aStoreFile, aStoreType, null, null );
	}

	/**
	 * Constructs a {@link KeyStoreDescriptor} with the data required to access
	 * a {@link KeyStore}.
	 * 
	 * @param aStoreFile The {@link KeyStore} {@link File}.
	 * @param aStoreType The {@link KeyStore}'s {@link StoreType}.
	 * @param aStorePassword The {@link KeyStore} password.
	 */
	public KeyStoreDescriptorImpl( File aStoreFile, StoreType aStoreType, String aStorePassword ) {
		this( aStoreFile, aStoreType, aStorePassword, null );
	}

	/**
	 * Constructs a {@link KeyStoreDescriptor} with the data required to access
	 * a {@link KeyStore}.
	 * 
	 * @param aStoreFile The {@link KeyStore} {@link File}.
	 * @param aStorePassword The {@link KeyStore} password.
	 */
	public KeyStoreDescriptorImpl( File aStoreFile, String aStorePassword ) {
		this( aStoreFile, StoreType.JKS, aStorePassword, null );
	}

	/**
	 * Constructs a {@link KeyStoreDescriptor} with the data required to access
	 * a {@link KeyStore}.
	 * 
	 * @param aStoreFile The {@link KeyStore} {@link File}.
	 * @param aStorePassword The {@link KeyStore} password.
	 * @param aKeyPassword The key password.
	 */
	public KeyStoreDescriptorImpl( File aStoreFile, String aStorePassword, String aKeyPassword ) {
		this( aStoreFile, StoreType.JKS, aStorePassword, aKeyPassword );
	}

	/**
	 * Constructs a {@link KeyStoreDescriptor} with the data required to access
	 * a {@link KeyStore}.
	 * 
	 * @param aStoreFile The {@link KeyStore} {@link File}.
	 * @param aStoreType The {@link KeyStore}'s {@link StoreType}.
	 * @param aStorePassword The {@link KeyStore} password.
	 * @param aKeyPassword The key password.
	 */
	public KeyStoreDescriptorImpl( File aStoreFile, StoreType aStoreType, String aStorePassword, String aKeyPassword ) {
		_builder = new KeyStoreDescriptorBuilderImpl( aStoreFile, aStoreType, aStorePassword, aKeyPassword );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public File getStoreFile() {
		return _builder.getStoreFile();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StoreType getStoreType() {
		return _builder.getStoreType();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStorePassword() {
		return _builder.getStorePassword();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKeyPassword() {
		return _builder.getKeyPassword();
	}
}
