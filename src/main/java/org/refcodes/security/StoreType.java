// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

/**
 * The Enum StoreType.
 */
public enum StoreType {

	JKS,
	/**
	 * "... This keystore implementation employs a much stronger protection of
	 * private keys (using password-based encryption with Triple DES) than JKS.
	 * You can upgrade your keystore of type "JKS" to type "JCEKS" by changing
	 * the password of a private-key entry in your keystore ..." See
	 * "https://www.ibm.com/support/knowledgecenter/en/SSYKE2_7.1.0/com.ibm.java.security.component.71.doc/security-component/keytoolDocs/supportedkeystoretypes.html"
	 */
	JCEKS,
	/**
	 * "... There is a difference between PKCS12 type keystore created on the
	 * keytool provided in the IBM JVM and the keytool provided in an Oracle
	 * JVM. The keytool in an IBM JVM uses a PKCS12 keystore to store both key
	 * entries and certificate entries.The keytool in an Oracle JVM uses a
	 * PKCS12 keystore to store key entries. The keytool program in IBM's JVM
	 * can read the keystore created by the keytool program provided by an
	 * Oracle JVM, but not the other way around ..." See
	 * "https://www.ibm.com/support/knowledgecenter/en/SSYKE2_7.1.0/com.ibm.java.security.component.71.doc/security-component/keytoolDocs/supportedkeystoretypes.html"
	 */
	PKCS12,
	/**
	 * "... This is a second version of PKCS12 type keystore. It can be read by
	 * the keytool program in an Oracle JVM ..." See
	 * "https://www.ibm.com/support/knowledgecenter/en/SSYKE2_7.1.0/com.ibm.java.security.component.71.doc/security-component/keytoolDocs/supportedkeystoretypes.html"
	 */
	PKCS12S2,
	/**
	 * "... This is a RACF keyring keystore. This type is available only on z/OS
	 * systems with RACF installed ..." See
	 * "https://www.ibm.com/support/knowledgecenter/en/SSYKE2_7.1.0/com.ibm.java.security.component.71.doc/security-component/keytoolDocs/supportedkeystoretypes.html"
	 */
	JCERACFKS;

}
