// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

import java.io.File;
import java.security.KeyStore;

/**
 * Describes the data required to access a truststore. A truststore stores
 * certificates from third party to identify third party and signed by
 * certificate authorities such as "Verisign", "Thawte", or "GoDaddy". A client
 * will use certificates stored in a truststore to verify the identity of a
 * server.
 */
public interface TrustStoreDescriptor {

	/**
	 * Retrieves the {@link File} pointing to the {@link KeyStore}.
	 * 
	 * @return The {@link KeyStore} {@link File}.
	 */
	File getStoreFile();

	/**
	 * Retrieves the {@link StoreType} representing the type of the
	 * {@link KeyStore}.
	 * 
	 * @return The {@link KeyStore}'s {@link StoreType}.
	 */
	StoreType getStoreType();

	/**
	 * Retrieves the keysotre's password.
	 * 
	 * @return The {@link KeyStore} password.
	 */
	String getStorePassword();

	/**
	 * The mutable {@link TrustStoreDescriptorBuilder} extends the
	 * {@link TrustStoreDescriptor} with manipulator methods.
	 */
	public static interface TrustStoreDescriptorBuilder extends TrustStoreDescriptor {
		/**
		 * Sets the {@link File} pointing to the {@link KeyStore}.
		 * 
		 * @param aStoreFile The {@link KeyStore} {@link File}.
		 */
		void setStoreFile( File aStoreFile );

		/**
		 * Builder for the according setter method.
		 * 
		 * @param aStoreFile The {@link KeyStore} {@link File}.
		 * 
		 * @return The builder instance as of the builder pattern.
		 */
		default TrustStoreDescriptorBuilder withStoreFile( File aStoreFile ) {
			setStoreFile( aStoreFile );
			return this;
		}

		/**
		 * Sets the {@link StoreType} representing the type of the
		 * {@link KeyStore}.
		 * 
		 * @param aStoreType The {@link KeyStore}'s {@link StoreType}.
		 */
		void setStoreType( StoreType aStoreType );

		/**
		 * Builder for the according setter method.
		 * 
		 * @param aStoreType The {@link KeyStore}'s {@link StoreType}.
		 * 
		 * @return The builder instance as of the builder pattern.
		 */
		default TrustStoreDescriptorBuilder withStoreType( StoreType aStoreType ) {
			setStoreType( aStoreType );
			return this;
		}

		/**
		 * Sets the keysotre's password.
		 * 
		 * @param aStorePassword The {@link KeyStore} password.
		 */
		void setStorePassword( String aStorePassword );

		/**
		 * Builder for the according setter method.
		 * 
		 * @param aStorePassword The {@link KeyStore} password.
		 * 
		 * @return The builder instance as of the builder pattern.
		 */
		default TrustStoreDescriptorBuilder withStorePassword( String aStorePassword ) {
			setStorePassword( aStorePassword );
			return this;
		}
	}
}
