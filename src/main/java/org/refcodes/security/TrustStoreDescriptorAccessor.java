// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

/**
 * Provides an accessor for a {@link TrustStoreDescriptor} property.
 */
public interface TrustStoreDescriptorAccessor {

	/**
	 * Retrieves the {@link TrustStoreDescriptor} from the
	 * {@link TrustStoreDescriptor} property.
	 * 
	 * @return The {@link TrustStoreDescriptor} stored by the
	 *         {@link TrustStoreDescriptor} property.
	 */
	TrustStoreDescriptor getTrustStoreDescriptor();

	/**
	 * Provides a mutator for a {@link TrustStoreDescriptor} property.
	 */
	public interface TrustStoreDescriptorMutator {

		/**
		 * Sets the {@link TrustStoreDescriptor} for the
		 * {@link TrustStoreDescriptor} property.
		 * 
		 * @param aTrustStoreDescriptor The {@link TrustStoreDescriptor} to be
		 *        stored by the {@link TrustStoreDescriptor} property.
		 */
		void setTrustStoreDescriptor( TrustStoreDescriptor aTrustStoreDescriptor );
	}

	/**
	 * Provides a builder method for a {@link TrustStoreDescriptor} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TrustStoreDescriptorBuilder<B extends TrustStoreDescriptorBuilder<B>> {

		/**
		 * Sets the {@link TrustStoreDescriptor} for the
		 * {@link TrustStoreDescriptor} property.
		 * 
		 * @param aTrustStoreDescriptor The {@link TrustStoreDescriptor} to be
		 *        stored by the {@link TrustStoreDescriptor} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTrustStoreDescriptor( TrustStoreDescriptor aTrustStoreDescriptor );
	}

	/**
	 * Provides a {@link TrustStoreDescriptor} property.
	 */
	public interface TrustStoreDescriptorProperty extends TrustStoreDescriptorAccessor, TrustStoreDescriptorMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link TrustStoreDescriptor} (setter) as of
		 * {@link #setTrustStoreDescriptor(TrustStoreDescriptor)} and returns
		 * the very same value (getter).
		 * 
		 * @param aTrustStoreDescriptor The {@link TrustStoreDescriptor} to set
		 *        (via {@link #setTrustStoreDescriptor(TrustStoreDescriptor)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default TrustStoreDescriptor letTrustStoreDescriptor( TrustStoreDescriptor aTrustStoreDescriptor ) {
			setTrustStoreDescriptor( aTrustStoreDescriptor );
			return aTrustStoreDescriptor;
		}
	}
}
