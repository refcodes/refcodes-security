// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

import java.io.File;

import org.refcodes.security.TrustStoreDescriptor.TrustStoreDescriptorBuilder;

/**
 * Implementation of the {@link KeyStoreDescriptor} interface.
 */
public class TrustStoreDescriptorBuilderImpl implements TrustStoreDescriptorBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private File _storeFile;
	private StoreType _storeType;
	private String _storePassword = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link KeyStoreDescriptor} with the data required to access
	 * a keystore. The {@link StoreType} defaults to {@link StoreType#JKS}.
	 * 
	 * @param aStoreFile The keystore {@link File}.
	 */
	public TrustStoreDescriptorBuilderImpl( File aStoreFile ) {
		this( aStoreFile, StoreType.JKS );
	}

	/**
	 * Constructs a {@link KeyStoreDescriptor} with the data required to access
	 * a keystore.
	 * 
	 * @param aStoreFile The keystore {@link File}.
	 * @param aStoreType The keystore's {@link StoreType}.
	 */
	public TrustStoreDescriptorBuilderImpl( File aStoreFile, StoreType aStoreType ) {
		this( aStoreFile, aStoreType, null );
	}

	/**
	 * Constructs a {@link KeyStoreDescriptor} with the data required to access
	 * a keystore.
	 * 
	 * @param aStoreFile The keystore {@link File}.
	 * @param aStorePassword The keystore password.
	 */
	public TrustStoreDescriptorBuilderImpl( File aStoreFile, String aStorePassword ) {
		this( aStoreFile, StoreType.JKS, aStorePassword );
	}

	/**
	 * Constructs a {@link KeyStoreDescriptor} with the data required to access
	 * a keystore.
	 * 
	 * @param aStoreFile The keystore {@link File}.
	 * @param aStoreType The keystore's {@link StoreType}.
	 * @param aStorePassword The keystore password.
	 */
	public TrustStoreDescriptorBuilderImpl( File aStoreFile, StoreType aStoreType, String aStorePassword ) {
		_storeFile = aStoreFile;
		_storeType = aStoreType;
		_storePassword = aStorePassword;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public File getStoreFile() {
		return _storeFile;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStoreFile( File aStoreFile ) {
		_storeFile = aStoreFile;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StoreType getStoreType() {
		return _storeType;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStoreType( StoreType aStoreType ) {
		_storeType = aStoreType;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStorePassword() {
		return _storePassword;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStorePassword( String aStorePassword ) {
		_storePassword = aStorePassword;
	}
}
