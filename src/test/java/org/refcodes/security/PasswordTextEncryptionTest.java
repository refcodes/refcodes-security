// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.refcodes.data.Text;
import org.refcodes.runtime.SystemProperty;

public class PasswordTextEncryptionTest {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testPasswordTextEncryption1() throws EncryptionException, DecryptionException {
		final String thePassword = "!!!TopSecret!!!";
		final String theOriginal = "Hello World!";
		final PasswordTextEncrypter theEncrypter = new PasswordTextEncrypter( thePassword );
		final PasswordTextDecrypter theDecrypter = new PasswordTextDecrypter( thePassword );
		final String theEncrypted = theEncrypter.toEncrypted( theOriginal );
		final String theDecrypted = theDecrypter.toDecrypted( theEncrypted );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Original = " + theOriginal );
			System.out.println( "Encrypted = " + theEncrypted );
			System.out.println( "Decrypted = " + theDecrypted );
		}
		assertNotEquals( theOriginal, theEncrypted );
		assertEquals( theOriginal, theDecrypted );
	}

	@Test
	public void testPasswordTextEncryption2() throws EncryptionException, DecryptionException {
		final String thePassword = "qwertzuiop1234567890!?";
		final String theOriginal = Text.ARECIBO_MESSAGE.getText();
		final PasswordTextEncrypter theEncrypter = new PasswordTextEncrypter( thePassword );
		final PasswordTextDecrypter theDecrypter = new PasswordTextDecrypter( thePassword );
		final String theEncrypted = theEncrypter.toEncrypted( theOriginal );
		final String theDecrypted = theDecrypter.toDecrypted( theEncrypted );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Original = " + theOriginal );
			System.out.println( "Encrypted = " + theEncrypted );
			System.out.println( "Decrypted = " + theDecrypted );
		}
		assertNotEquals( theOriginal, theEncrypted );
		assertEquals( theOriginal, theDecrypted );
	}

}
